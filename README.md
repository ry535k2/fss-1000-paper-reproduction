# Paper Reproduction

This repo is part of an unofficial participation at ML [Reproducibility Challenge 2020](https://paperswithcode.com/rc2020)
as the task for ML2 course at UL FRI.

I selected paper __FSS-1000: A 1000-Class Dataset for Few-Shot Segmentation__ by Xiang Li et al, published
in CVPR 2020.

For the description of the analysis and results see [the report](./report.pdf).

### Data

To run the code you will need FSS-1000 dataset. Place it under `data` folder with the following structure: 

```
data
  fss-1000
    few-shot
      0
        folders with names of classes for 1st fold
          images with .jpg ending and corresponding .png segmentation
      1
        folders with names of classes for 2nd fold
          images with .jpg ending and corresponding .png segmentation
      2
        folders with names of classes for 3rd fold
          images with .jpg ending and corresponding .png segmentation
      3
        folders with names of classes for 4th fold
          images with .jpg ending and corresponding .png segmentation
```

### Code

Code is in `src` folder. 

Change `src/helpers/constants.py` for different configuration.

Run by running `src/run.py` and passing fold number as the argument. Example:

```
cd src
python3 run.py 0
```
