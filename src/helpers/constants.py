import torch
from math import pi, sqrt
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim
import torchvision
import torchvision.transforms as transforms
import helpers.time_analysis as time
import yaml
import torch.nn.functional as F
from torch import tensor, unsqueeze
from torch.nn import Parameter
import os
from datetime import datetime
import sys
from pprint import pprint
from copy import deepcopy
import math
import traceback
from PIL import Image, ImageFile
import cv2
from tqdm import tqdm
from torch.utils.data import Dataset, DataLoader
import random
import inspect
from shutil import copy as copyfile

torch.manual_seed(0)


class Constants:
    lr = .001
    batch_size = 16
    epochs = 100000
    limit_input_categories_per_fold = None
    debug = False
    load_model = False
    save_model = True
    calc_mesh_iou = False
    model_name = '19'

    @staticmethod
    def print():
        print('PARAMETERS')
        attributes = inspect.getmembers(Constants, lambda a: not (inspect.isroutine(a)))
        for attribute in attributes:
            if not attribute[0].startswith('__') and not attribute[0].endswith('__'):
                print(f'{attribute[0]:40}: {attribute[1]}')


if not Constants.debug:
    assert torch.cuda.is_available()
    device = torch.device('cuda:0')
else:
    device = torch.device('cpu')
    torch.autograd.set_detect_anomaly(True)

ImageFile.LOAD_TRUNCATED_IMAGES = True


def draw_grad_graph(grad_fn, depth=0):
    if type(grad_fn) in [torch.nn.parameter.Parameter, torch.Tensor]:
        grad_fn = grad_fn.grad_fn
    if depth == 0:
        print('--- drawing autograd graph ---')

    tab = "".join(["  "] * depth)
    print(tab, grad_fn, sep="")

    depth += 1
    if grad_fn is not None:
        tab = "".join(["  "] * depth)
        for n in grad_fn.next_functions:
            if n[0]:
                if type(n[0]) == dict and 'variable' in n[0]:
                    print(tab, n[0], sep="")
                else:
                    draw_grad_graph(n[0], depth)

    if depth == 1:
        print()


def get_class(module, name):
    imp = __import__(module, fromlist=[name])
    return getattr(imp, name)


def imshow(img, cmap='gray', denorm=False):
    if cmap == 'color':  # also: hot
        cmap = 'nipy_spectral'
    if type(img) == torch.Tensor:
        img = img.cpu()
        img = img.squeeze()
        img = img.clone()
        if len(img.shape) == 3:
            if img.shape[0] == 3:
                img = img.permute(1, 2, 0)
                if denorm:
                    img *= torch.tensor([0.229, 0.224, 0.225])
                    img += torch.tensor([0.485, 0.456, 0.406])
        img = img.detach().numpy()

    if len(img.shape) == 3:
        plt.imshow(img)
        plt.show()
    else:
        plt.imshow(img)
        plt.set_cmap(cmap)
        plt.colorbar()
        plt.show()
