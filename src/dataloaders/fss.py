from helpers.constants import *


class Lister(Dataset):
    def __init__(self, fold: int, batch_size: int, train: bool, h):
        super(Lister, self).__init__()

        self.fold = fold
        self.batch_size = batch_size
        self.train = train
        self.h = h
        self.counter = -1
        self.images = {}
        resize = transforms.Resize((self.h, self.h), interpolation=Image.NEAREST)

        for fold in range(4):
            if train and fold == self.fold or not train and fold != self.fold:
                continue
            print('Reading fold', fold)

            for cat in os.listdir(f'../data/fss-1000/few-shot/{fold}'):
                if Constants.debug and len(self.images) >= 3:
                    break
                if Constants.limit_input_categories_per_fold is not None and len(self.images) >= Constants.limit_input_categories_per_fold:
                    break

                self.images[cat] = []
                for file in os.listdir(f'../data/fss-1000/few-shot/{fold}/{cat}'):
                    if file.endswith('.jpg'):
                        img = Image.open(f'../data/fss-1000/few-shot/{fold}/{cat}/{file}')
                        img = img.resize((self.h, self.h))
                        img = torch.tensor(np.array(img), dtype=torch.float32) / 255
                        img = img.permute(2, 0, 1)
                        img = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])(img)  # conflict paper with code

                        gt = torch.tensor(np.array(Image.open(f'../data/fss-1000/few-shot/{fold}/{cat}/{file[:-4]}.png')),
                                          dtype=torch.float32)[..., 0] / 255
                        gt = resize(gt.unsqueeze(0)).squeeze()

                        img1 = torch.cat((img, gt.unsqueeze(0)), 0)
                        img2 = torch.cat((img, torch.zeros(1, self.h, self.h)), 0)

                        self.images[cat].append((img1, img2, gt))

        self.categories = list(self.images.keys())

    def __len__(self):
        if self.train:
            return self.batch_size
        return len(self.categories)

    def __getitem__(self, i):
        self.counter += 1
        if self.train:
            cat = self.categories[self.counter % len(self.categories)]
        else:
            cat = self.categories[i]

        random.seed(self.counter)
        sfile, qfile = random.sample(self.images[cat], 2)
        return sfile[0], qfile[1], qfile[2]

    def reset_randomness(self):
        self.counter = -1


class Data:
    def __init__(self, *, fold, batch_size=128):
        self.fold = fold
        self.batch_size = batch_size
        self.shape = 3, 224, 224

        self.trainset = Lister(fold, batch_size, train=True, h=self.shape[1])
        self.train = DataLoader(self.trainset, batch_size=batch_size, shuffle=False, num_workers=0, pin_memory=True)

        self.testset = Lister(fold, batch_size, train=False, h=self.shape[1])
        self.test = DataLoader(self.testset, batch_size=batch_size, shuffle=False, num_workers=0, pin_memory=True)
