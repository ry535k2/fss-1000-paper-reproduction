from helpers.constants import *


class Base:
    def __init__(self, *, Net, Data, fold):
        self.name = f'{Constants.model_name}, fold: {fold}'
        if not Constants.load_model and Constants.save_model:
            assert not os.path.exists(f'output/weights/{self.name}.pth')

        if Constants.debug:
            self.data = Data(fold=fold, batch_size=2)
        else:
            self.data = Data(fold=fold, batch_size=Constants.batch_size)

        self.net = Net(input_shape=self.data.shape).to(device)
        self.lr = Constants.lr
        self.batch_size = self.data.batch_size
        self.no_train = len(self.data.train.dataset)
        self.no_test = len(self.data.test.dataset)
        self.no_batches = self.no_train // self.batch_size
        self.optimizer = None

        self.epochs = Constants.epochs
        self.epochs //= self.batch_size
        self.lr = Constants.lr

        # printing
        print('Device:', device)
        print()
        print('Network:', self.net)
        print()
        Constants.print()
        print()

    def imshow(self, img):
        img = img / 2 + 0.5
        npimg = img.cpu().numpy()
        if len(npimg.shape) == 3:
            npimg = np.transpose(npimg, (1, 2, 0))
        plt.imshow(npimg)
        plt.show()

    def mean_iou(self, cycles=1, mesh=False):
        self.net.eval()
        if mesh:
            thrs = [-1, 0, .1, .2, .3, .4, .5, .6, .7, .8, .9, 1]
        else:
            thrs = [.5]
        with torch.no_grad():
            for thr in thrs:
                self.data.testset.reset_randomness()
                iousa = torch.zeros(len(self.data.testset.categories), dtype=torch.int64, device=device)
                iousb = torch.zeros(len(self.data.testset.categories), dtype=torch.int64, device=device)

                for _ in range(cycles):
                    for i, (support, query, gt) in enumerate(self.data.test):
                        support = support.to(device)
                        query = query.to(device)
                        gt = gt.to(device)
                        gt = gt.type(torch.bool)

                        outputs = self.net(support, query)
                        outputs = outputs > thr

                        b = self.data.batch_size
                        iousa[i * b:(i * b + gt.size(0))] += torch.sum(outputs * gt, dim=[1, 2])
                        iousb[i * b:(i * b + gt.size(0))] += torch.sum(torch.logical_or(outputs, gt), dim=[1, 2])

                mean_iou = torch.mean(iousa / iousb).item()
                if mesh:
                    print(thr, mean_iou)

        if mesh:
            exit()

        self.net.train()
        return mean_iou

    def save_params(self, epoch):
        torch.save({
            'epoch': epoch,
            'net': self.net.state_dict(),
            'optimizer': self.optimizer.state_dict(),
            'counter train': self.data.trainset.counter,
            'counter test': self.data.testset.counter,
        }, f'output/weights/{self.name}.pth')

    def load_params(self):
        data = torch.load(f'output/weights/{self.name}.pth')
        self.net.load_state_dict(data['net'])
        self.optimizer.load_state_dict(data['optimizer'])
        self.data.trainset.counter = data['counter train']
        self.data.testset.counter = data['counter test']
        return data['epoch']

    def train(self):
        time.start('train')

        criterion = nn.BCELoss()  # conflict paper with code
        self.optimizer = optim.Adam(self.net.parameters(), lr=self.lr)

        last_epoch = -1
        if Constants.load_model:
            last_epoch = self.load_params()

        scheduler = optim.lr_scheduler.StepLR(self.optimizer, max(100, self.epochs // 10), gamma=.5, last_epoch=last_epoch)

        if Constants.calc_mesh_iou:
            self.mean_iou(mesh=True)

        self.net.train()
        # max_iou = 0

        for epoch in range(last_epoch + 1, self.epochs):
            loss_sum = 0
            time.start('epoch')

            for support, query, gt in self.data.train:
                assert support.is_pinned() and query.is_pinned() and gt.is_pinned()
                support = support.to(device)
                query = query.to(device)
                gt = gt.to(device)

                # calculate error
                outputs = self.net(support, query)
                loss = criterion(outputs, gt)

                # change weights
                self.optimizer.zero_grad()
                loss.backward()
                # torch.nn.utils.clip_grad_norm_(self.net.parameters(), 0.5)  # conflict paper with code
                self.optimizer.step()

                loss_sum += loss.item()

            scheduler.step()

            print(f'epoch: {epoch:3}/{self.epochs}, loss: {loss_sum:.6f}, time: {time.state("epoch"):2.1f} s '
                  f'({time.state("train") / 60:.0f} min total, '
                  f'{time.state("train") / 60 / (epoch - last_epoch) * (self.epochs - epoch):.0f} min remaining)')

            # saving model
            if (epoch + 1) % 100 == 0:
                mean_iou = self.mean_iou()
                # if mean_iou > max_iou:
                #     max_iou = mean_iou
                if Constants.save_model:
                    self.save_params(epoch)
                print(f'       mean iou: {mean_iou:.4f}')
                sys.stdout.flush()

        print(f'final mean iou: {self.mean_iou(cycles=10)}')

        time.end('train')
        time.print_()
