from helpers.constants import *

names_map = {
    '13': 'model A',
    '14': 'model B',
    '16': 'model C',
    '17': 'model D',
}

with open(f'loss.csv', 'w+') as loss_file:
    with open(f'iou.csv', 'w+') as iou_file:
        loss_file.write('epoch,loss,model\n')
        iou_file.write('epoch,mIoU,model\n')
        for name in names_map:
            skip = 30
            past = []
            avg = 50
            epoch = 0

            with open(f'../output/{name}') as inp_file:
                for line in inp_file.readlines():
                    line = line.strip()

                    if line.startswith('final'):
                        break
                    elif line.startswith('epoch:'):
                        skip -= 1
                        if skip > 0:
                            continue

                        epoch, loss = line.split(', loss: ')
                        epoch = epoch.split('epoch:')[1].strip()
                        epoch = epoch.split('/')[0]
                        epoch = int(epoch) * 16

                        loss = float(loss.split(', ')[0])
                        past.append(loss)
                        if len(past) > avg:
                            past.pop(0)
                            loss_file.write(f'{epoch},{sum(past) / len(past)},{names_map[name]}\n')
                    elif line.startswith('mean iou'):
                        iou = line.split('mean iou: ')[1]
                        iou_file.write(f'{epoch},{iou},{names_map[name]}\n')
