from helpers.constants import *

folds = []
for fold in range(4):
    cat = list(os.listdir(f'../../data/fss-1000/few-shot/{fold}'))
    for i in range(len(cat)):
        cat[i] = cat[i].replace('_', ' ')
    folds.append(cat)

a = folds
for i in range(250):
    print(f'{a[0][i]} & {a[1][i]} & {a[2][i]} & {a[3][i]} \\\\')

