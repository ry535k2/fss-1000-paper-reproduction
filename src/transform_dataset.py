from helpers.constants import *

for fold in range(1, 4):
    for cat in tqdm(os.listdir(f'../data/fss-1000/few-shot/{fold}')):
        if not os.path.exists(f'../data/fss-1000/format2/support/{cat}'):
            os.mkdir(f'../data/fss-1000/format2/support/{cat}')
            os.mkdir(f'../data/fss-1000/format2/support/{cat}/image')
            os.mkdir(f'../data/fss-1000/format2/support/{cat}/label')
        for file in os.listdir(f'../data/fss-1000/few-shot/{fold}/{cat}'):
            if file.endswith('.jpg'):
                img = Image.open(f'../data/fss-1000/few-shot/{fold}/{cat}/{file}')
                img = img.resize((224, 224))
                img.save(f'../data/fss-1000/format2/support/{cat}/image/{file}')

                img = Image.open(f'../data/fss-1000/few-shot/{fold}/{cat}/{file[:-4]}.png')
                img = img.resize((224, 224))
                img.save(f'../data/fss-1000/format2/support/{cat}/label/{file[:-4]}.png')


for cat in os.listdir(f'../data/fss-1000/format2/support'):
    assert len(list(os.listdir(f'../data/fss-1000/format2/support/{cat}/label'))) == 10, cat
    assert len(list(os.listdir(f'../data/fss-1000/format2/support/{cat}/image'))) == 10, cat
