from base import *

from models.seg import Seg
from dataloaders.fss import Data

if __name__ == '__main__':
    print('start:', datetime.now())
    fold = 0
    if len(sys.argv) >= 2:
        fold = int(sys.argv[1])

    print('\nSTARTING FOLD:', fold)
    net = Base(
        Net=Seg,
        Data=Data,
        fold=fold,
    )
    net.train()

    print('\nfinish:', datetime.now())
