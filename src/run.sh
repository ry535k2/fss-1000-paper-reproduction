#!/usr/bin/env bash

set -e

printf "\n\n============================================================================================================================================\n\n" \
  2>&1 | tee -a /home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src/output/stdout

export PYTHONPATH=$PYTHONPATH:/home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src

python3 run.py 0 2>&1 | tee -a /home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src/output/stdout
python3 run.py 1 2>&1 | tee -a /home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src/output/stdout
python3 run.py 2 2>&1 | tee -a /home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src/output/stdout
python3 run.py 3 2>&1 | tee -a /home/v/Documents/gitlab/faks/ML2/reprodukcija/fss-1000-paper-reproduction/src/output/stdout
