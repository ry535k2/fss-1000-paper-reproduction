from helpers.constants import *


class Seg(nn.Module):
    def __init__(self, input_shape):
        super(Seg, self).__init__()
        self.input_shape = input_shape

        self.first_layer = nn.Conv2d(4, 64, kernel_size=3, padding=1)

        # vgg
        vgg = torchvision.models.vgg16_bn(pretrained=True).features
        vgg_first, self.vgg = vgg[0], vgg[1:]
        self.first_layer.weight.data[:, 3] = 0  # conflict paper with code
        self.first_layer.weight.data[:, :3] = vgg_first.weight.data  # conflict paper with code

        self.relation = nn.Sequential(
            nn.Conv2d(1024, 512, kernel_size=1, padding=0),  # conflict paper with code
            nn.BatchNorm2d(512),  # conflict paper with code
            nn.ReLU(),
            nn.Conv2d(512, 512, kernel_size=1, padding=0),  # conflict paper with code
            nn.BatchNorm2d(512),  # conflict paper with code
            nn.ReLU()
        )
        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)  # conflict paper with code
        self.decoder0 = nn.Sequential(
            nn.Conv2d(1024, 512, kernel_size=3, padding=1),
            nn.BatchNorm2d(512),  # conflict paper with code
            nn.ReLU(),
        )
        self.decoder1 = nn.Sequential(
            nn.Conv2d(1024, 256, kernel_size=3, padding=1),
            nn.BatchNorm2d(256),  # conflict paper with code
            nn.ReLU(),
        )
        self.decoder2 = nn.Sequential(
            nn.Conv2d(512, 128, kernel_size=3, padding=1),
            nn.BatchNorm2d(128),  # conflict paper with code
            nn.ReLU(),
        )
        self.decoder3 = nn.Sequential(
            nn.Conv2d(256, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),  # conflict paper with code
            nn.ReLU(),
        )
        self.decoder4 = nn.Sequential(
            nn.Conv2d(128, 64, kernel_size=3, padding=1),
            nn.BatchNorm2d(64),  # conflict paper with code
            nn.ReLU()
        )
        self.last_conv = nn.Conv2d(64, 1, kernel_size=3, padding=1)  # conflict paper with code

        # init weights
        nn.Sequential(
            self.relation,
            self.decoder0,
            self.decoder1,
            self.decoder2,
            self.decoder3,
            self.decoder4,
            self.last_conv
        ).apply(self.init_weights)

    def init_weights(self, module):
        if type(module) == nn.Conv2d:
            n = module.kernel_size[0] * module.kernel_size[1] * module.out_channels
            module.weight.data.normal_(0, math.sqrt(2. / n))
            if module.bias is not None:
                module.bias.data.zero_()

    def embed(self, x, make_skips=False):
        x = self.first_layer(x)
        skips = []
        for i, layer in enumerate(self.vgg):
            x = layer(x)
            if make_skips and i in [4, 11, 21, 31, 41]:
                skips.append(x)
        if make_skips:
            return x, skips
        return x

    def forward(self, support, query):

        # encoding
        support = self.embed(support)
        query, skips = self.embed(query, make_skips=True)

        # concatenating support and query
        features = torch.cat((support, query), dim=1)

        # relation module
        y = self.relation(features)

        # decoder
        y = self.upsample(y)
        y = torch.cat((y, skips[4]), dim=1)
        y = self.decoder0(y)

        y = self.upsample(y)
        y = torch.cat((y, skips[3]), dim=1)
        y = self.decoder1(y)

        y = self.upsample(y)
        y = torch.cat((y, skips[2]), dim=1)
        y = self.decoder2(y)

        y = self.upsample(y)
        y = torch.cat((y, skips[1]), dim=1)
        y = self.decoder3(y)

        y = self.upsample(y)
        y = torch.cat((y, skips[0]), dim=1)
        y = self.decoder4(y)

        y = self.last_conv(y)

        y.squeeze_()
        y = torch.sigmoid(y)

        return y
